<?php
namespace Fhkonzept\GigasetCasemanager\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Christian Reifenscheid <reifenscheid@fh-konzept.de>, fh-konzept.de
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Usecase
 */
class Usecase extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * headline
	 *
	 * @var string
	 */
	protected $headline = '';

	/**
	 * headlineLayout
	 *
	 * @var int
	 */
	protected $headlineLayout = 0;

	/**
	 * subtitle
	 *
	 * @var string
	 */
	protected $subtitle = '';

	/**
	 * bodytext
	 *
	 * @var string
	 */
	protected $bodytext = '';

	/**
	 * imageposition
	 *
	 * @var int
	 */
	protected $imageposition = 0;

	/**
	 * image
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	protected $image = NULL;

	/**
	 * Returns the headline
	 *
	 * @return string $headline
	 */
	public function getHeadline() {
		return $this->headline;
	}

	/**
	 * Sets the headline
	 *
	 * @param string $headline
	 * @return void
	 */
	public function setHeadline($headline) {
		$this->headline = $headline;
	}

	/**
	 * Returns the headlineLayout
	 *
	 * @return int $headlineLayout
	 */
	public function getHeadlineLayout() {
		return $this->headlineLayout;
	}

	/**
	 * Sets the headlineLayout
	 *
	 * @param int $headlineLayout
	 * @return void
	 */
	public function setHeadlineLayout($headlineLayout) {
		$this->headlineLayout = $headlineLayout;
	}

	/**
	 * Returns the subtitle
	 *
	 * @return string $subtitle
	 */
	public function getSubtitle() {
		return $this->subtitle;
	}

	/**
	 * Sets the subtitle
	 *
	 * @param string $subtitle
	 * @return void
	 */
	public function setSubtitle($subtitle) {
		$this->subtitle = $subtitle;
	}

	/**
	 * Returns the bodytext
	 *
	 * @return string $bodytext
	 */
	public function getBodytext() {
		return $this->bodytext;
	}

	/**
	 * Sets the bodytext
	 *
	 * @param string $bodytext
	 * @return void
	 */
	public function setBodytext($bodytext) {
		$this->bodytext = $bodytext;
	}

	/**
	 * Returns the imageposition
	 *
	 * @return int $imageposition
	 */
	public function getImageposition() {
		return $this->imageposition;
	}

	/**
	 * Sets the imageposition
	 *
	 * @param int $imageposition
	 * @return void
	 */
	public function setImageposition($imageposition) {
		$this->imageposition = $imageposition;
	}

	/**
	 * Returns the image
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Sets the image
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
	 * @return void
	 */
	public function setImage($image) {
		$this->image = $image;
	}

}