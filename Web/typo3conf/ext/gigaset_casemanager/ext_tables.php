<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'Fhkonzept.' . $_EXTKEY,
	'Casemanager',
	'Gigaset - Case Manager'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Gigaset - Case Manager');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_gigasetcasemanager_domain_model_usecase', 'EXT:gigaset_casemanager/Resources/Private/Language/locallang_csh_tx_gigasetcasemanager_domain_model_usecase.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_gigasetcasemanager_domain_model_usecase');
