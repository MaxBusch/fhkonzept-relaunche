<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Fhkonzept.' . $_EXTKEY,
	'Casemanager',
	array(
		'Usecase' => 'list',
		
	),
	// non-cacheable actions
	array(
		'Usecase' => '',
		
	)
);
