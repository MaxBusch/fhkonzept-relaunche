<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:gigaset_casemanager/Resources/Private/Language/locallang_db.xlf:tx_gigasetcasemanager_domain_model_usecase',
		'label' => 'headline',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'headline,subtitle,bodytext,imageposition,image,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('gigaset_casemanager') . 'Resources/Public/Icons/tx_gigasetcasemanager_domain_model_usecase.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, headline, headline_layout, subtitle, bodytext, imageposition, image',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, headline, headline_layout, subtitle, bodytext, imageposition, image, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_gigasetcasemanager_domain_model_usecase',
				'foreign_table_where' => 'AND tx_gigasetcasemanager_domain_model_usecase.pid=###CURRENT_PID### AND tx_gigasetcasemanager_domain_model_usecase.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'headline' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:gigaset_casemanager/Resources/Private/Language/locallang_db.xlf:tx_gigasetcasemanager_domain_model_usecase.headline',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),

		'headline_layout' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:gigaset_casemanager/Resources/Private/Language/locallang_db.xlf:tx_gigasetcasemanager_domain_model_usecase.headline.layout',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('LLL:EXT:gigaset_casemanager/Resources/Private/Language/locallang_db.xlf:tx_gigasetcasemanager_domain_model_usecase.headline.layout.paragraph', 0),
					array('LLL:EXT:gigaset_casemanager/Resources/Private/Language/locallang_db.xlf:tx_gigasetcasemanager_domain_model_usecase.headline.layout.h1', 1),
					array('LLL:EXT:gigaset_casemanager/Resources/Private/Language/locallang_db.xlf:tx_gigasetcasemanager_domain_model_usecase.headline.layout.h2', 2),
					array('LLL:EXT:gigaset_casemanager/Resources/Private/Language/locallang_db.xlf:tx_gigasetcasemanager_domain_model_usecase.headline.layout.h3', 3),
					array('LLL:EXT:gigaset_casemanager/Resources/Private/Language/locallang_db.xlf:tx_gigasetcasemanager_domain_model_usecase.headline.layout.h4', 4),
					array('LLL:EXT:gigaset_casemanager/Resources/Private/Language/locallang_db.xlf:tx_gigasetcasemanager_domain_model_usecase.headline.layout.h5', 5),
					array('LLL:EXT:gigaset_casemanager/Resources/Private/Language/locallang_db.xlf:tx_gigasetcasemanager_domain_model_usecase.headline.layout.h6', 6),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'subtitle' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:gigaset_casemanager/Resources/Private/Language/locallang_db.xlf:tx_gigasetcasemanager_domain_model_usecase.subtitle',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'bodytext' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:gigaset_casemanager/Resources/Private/Language/locallang_db.xlf:tx_gigasetcasemanager_domain_model_usecase.bodytext',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			),
			'defaultExtras' => 'richtext[*]:rte_transform[mode=ts_css]'
		),
		'imageposition' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:gigaset_casemanager/Resources/Private/Language/locallang_db.xlf:tx_gigasetcasemanager_domain_model_usecase.imageposition',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('LLL:EXT:gigaset_casemanager/Resources/Private/Language/locallang_db.xlf:tx_gigasetcasemanager_domain_model_usecase.left', 0),
					array('LLL:EXT:gigaset_casemanager/Resources/Private/Language/locallang_db.xlf:tx_gigasetcasemanager_domain_model_usecase.right', 1),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),

		'image' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:gigaset_casemanager/Resources/Private/Language/locallang_db.xlf:tx_gigasetcasemanager_domain_model_usecase.image',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'image',
				array('maxitems' => 1,
					'appearance' => array(
						'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference'
					),
					'foreign_types' => array(
						'0' => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						)
					)
				),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),
		),
	),
);