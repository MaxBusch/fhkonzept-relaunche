<?php

if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

$extConf = unserialize($TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY]);

if ($extConf['logo_login']) {
	$TBE_STYLES['logo_login'] = $extConf['logo_login'];
}

# add static typoscript configuration
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'TYPO3 System Configuration');
