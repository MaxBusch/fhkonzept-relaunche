/**
 * Page: Template
 */
page = PAGE
page {
    10 = FLUIDTEMPLATE
    10 {
        templateRootPath = {$plugin.tx_system.view.templateRootPath}
		partialRootPath = {$plugin.tx_system.view.partialRootPath}
		layoutRootPath = {$plugin.tx_system.view.layoutRootPath}

		file = {$plugin.tx_system.view.templateRootPath}{$plugin.tx_system.view.template}
	}
}

[globalVar = TSFE:page|layout = 0]
	page.10.file = {$plugin.tx_system.view.templateRootPath}{$plugin.tx_system.view.template}
[globalVar = TSFE:page|layout = 1]
	page.10.file = {$plugin.tx_system.view.templateRootPath}Subpage.html
[globalVar = TSFE:page|layout = 2]
	page.10.file = {$plugin.tx_system.view.templateRootPath}Detail.html
[END]

#page.bodyTag > 
#page.bodyTagCObject = TEXT 
#page.bodyTagCObject.wrap = <body data-spy="scroll" data-target="#navbar"> 

/**
* Page: Config
*/
page {
    config {
        admPanel = 0
        locale_all = de_DE.UTF8
    }
}

/**
* Page: Meta
*/
page {
	meta {

    	# for inheritance from parent pages add the following fields
    	# in Install Tool at [FE][addRootLineFields]

    	# application name
    	application-name = Webpage
    	# keywords
    	keywords.data = page:keywords
    	keywords.ifEmpty.data = levelfield :-1, keywords, slide
    	keywords.keywords = 1
    	# description
    	description.data = page:description
    	description.ifEmpty.data = levelfield :-1, description, slide
    	# author
    	author.data = page:author
    	author.ifEmpty.data = levelfield :-1, author, slide
    	# author email
    	author_email.data = page:author_email
    	author_email.ifEmpty.data = levelfield :-1, author_email, slide
    	# abstract
    	# not registered in html5
    	#abstract.data = page:abstract
    	#abstract.ifEmpty.data = levelfield :-1, abstract, slide
    	# publisher
    	publisher.data = page:author
    	publisher.ifEmpty.data = levelfield :-1, author, slide
    	# copyright
    	copyright = {$plugin.tx_system.settings.copyright}
    	# language
    	language.data = TSFE:lang
    	# robots
    	robots = index, follow
    	# revisit after
    	revisit-after = 7 days
        # last modified
    	last-modified.data = page:SYS_LASTCHANGED
    	last-modified.date = c
    }

    headerData {

    	# metas & icons
    	1 = TEXT
    	1.value (
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="white">

<meta name="format-detection" content="telephone=no">
<meta http-equiv="x-rim-auto-match" content="none">

<link rel="apple-touch-icon" href="/typo3conf/ext/system/Resources/Public/Images/72.jpg" /> 
<link rel="apple-touch-icon" sizes="72x72" href="/typo3conf/ext/system/Resources/Public/Images/72.jpg" /> 
<link rel="apple-touch-icon" sizes="114x114" href="/typo3conf/ext/system/Resources/Public/Images/114.jpg" /> 
<link rel="apple-touch-icon" sizes="144x144" href="/typo3conf/ext/system/Resources/Public/Images/144.jpg" /> 
<link rel="icon" href="/typo3conf/ext/system/Resources/Public/Images/favicon.ico" /> 
<link rel="shortcut icon" type="image/x-icon" href="/typo3conf/ext/system/Resources/Public/Images/favicon.ico" />
    	)
    }
}

/**
* Page: JavaScript & Style Libs
*/
page {
    javascriptLibs {
        # Note: All jQuery-related options are available since TYPO3 v6.0
        # include jQuery (boolean)
        jQuery = 1

        # Change the version
        # (possible values: latest|1.7.2|…, default: latest)
        # Note: jQuery.source has to be a CDN like "google"
        # when jQuery.version is not "latest"
        jQuery.version = latest

        # Include from local or different CDNs
        # (possible values: local|google|jquery|msn, default: local)
        jQuery.source = local

        # Set jQuery into its own scope to avoid conflicts (boolean)
        jQuery.noConflict = 0

        # Change the namespace when noConflict is activated
        # and use jQuery with "TYPO3.###NAMESPACE###(…);"
        # (string, default: jQuery)
        #jQuery.noConflict.namespace = ownNamespace

        # prototype
        Prototype = 0

        # Scriptaculous
        Scriptaculous = 0

        #  Scriptaculous Module hinzufügen
        Scriptaculous.modules = dragdrop,controls

        # ExtCore
        ExtCore = 0

        # die ExtCore Debug-Datei (unkomprimiert)
        ExtCore.debug = 0

        # ExtJS
        ExtJs = 0

        # ext-all.css
        ExtJs.css = 0

        # standard theme
        ExtJs.theme = 0

        # läd angegebenen Adapter (jquery|prototype|yui)
        ExtJs.adapter = jquery

        # QuickTips
        ExtJs.quickTips = 0

        # die ExtJS Debug-Datei (unkomprimiert)
        ExtJs.debug = 0
    }

    includeCSS {
        popup = {$plugin.tx_system.view.librariesRootPath}Magnific-Popup-master/dist/magnific-popup.css
        theme = {$plugin.tx_system.view.stylesheetsRootPath}styles.css
        slickslidertheme = {$plugin.tx_system.view.librariesRootPath}slickslider/slick-theme.css
        slickslider = {$plugin.tx_system.view.librariesRootPath}slickslider/slick.css
        modernizer-component = {$plugin.tx_system.view.librariesRootPath}modernizr/component.css
        fontawesome = {$plugin.tx_system.view.librariesRootPath}fontawesome/css/font-awesome.min.css
    }

    includeJSLibs {
        parallax = {$plugin.tx_system.view.librariesRootPath}parallax/parallax.js
        bootstrap = {$plugin.tx_system.view.librariesRootPath}bootstrap/js/bootstrap.min.js
        slickslider = {$plugin.tx_system.view.librariesRootPath}slickslider/slick.min.js
        modernizer = {$plugin.tx_system.view.librariesRootPath}modernizr/modernizr.min.js
        modernizer-custom = {$plugin.tx_system.view.librariesRootPath}modernizr/modernizr.custom.js
        respond = {$plugin.tx_system.view.librariesRootPath}respond/respond.min.js
    }

    includeJS {

    }

    includeJSFooterlibs {
        parallaxTheme = {$plugin.tx_system.view.javascriptsRootPath}init.parallax.js
        initSlick = {$plugin.tx_system.view.javascriptsRootPath}init.slickslider.js
        picturefill = {$plugin.tx_system.view.librariesRootPath}picturefill/dist/picturefill.min.js
        popup = {$plugin.tx_system.view.librariesRootPath}Magnific-Popup-master/dist/jquery.magnific-popup.min.js
    }

    includeJSFooter {
		gigaset = {$plugin.tx_system.view.javascriptsRootPath}gigaset-keeper.js
		detailPopup = {$plugin.tx_system.view.javascriptsRootPath}init.detail-popup.js
    }

    jsInline {

    }

    jsFooterInline {

    }
}