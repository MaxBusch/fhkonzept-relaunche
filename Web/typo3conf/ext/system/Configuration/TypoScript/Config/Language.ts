/**
* system
*
*
* @author  Carsten Walther
* @package system
* @version 6.4.0
*/

config {
    // Language Settings
    uniqueLinkVars = 1
    linkVars := addToList(L,*)

    sys_language_uid = 0

    sys_language_overlay = 1
    # sys_language_overlay = hideNonTranslated

    #sys_language_mode = strict
    sys_language_mode = content_fallback

    pageTitleFirst = 1

    language = de
    locale_all = de_DE
    htmlTag_langKey = de
}

[globalVar = GP:L = 0]
    config {
        language = de
        locale_all = de_DE
        htmlTag_langKey = de-DE
        sys_language_uid = 0
    }

    page.config.locale_all = de_DE.UTF8
[global]

[globalVar = GP:L = 1]
    config {
        language = en
        locale_all = en_EN
        htmlTag_langKey = en-EN
        sys_language_uid = 1
    }

    page.config.locale_all = en_EN.UTF8
[global]

[globalVar = GP:L = 2]
    config {
        language = fr
        locale_all = fr_FR
        htmlTag_langKey = fr-FR
        sys_language_uid = 2
    }

    page.config.locale_all = fr_FR.UTF8
[global]

[globalVar = GP:L = 3]
    config {
        language = pl
        locale_all = pl_PL
        htmlTag_langKey = pl-PL
        sys_language_uid = 3
    }

    page.config.locale_all = pl_PL.UTF8
[global]

[globalVar = GP:L = 4]
    config {
        language = es
        locale_all = es_ES
        htmlTag_langKey = es-ES
        sys_language_uid = 4
    }

    page.config.locale_all = es_ES.UTF8
[global]

[globalVar = GP:L = 5]
    config {
        language = nl
        locale_all = nl_NL
        htmlTag_langKey = nl-NL
        sys_language_uid = 5
    }

    page.config.locale_all = nl_NL.UTF8
[global]
