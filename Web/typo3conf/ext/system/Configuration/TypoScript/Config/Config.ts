config {

	baseURL = {$plugin.tx_system.settings.baseProtocol}://{$plugin.tx_system.settings.baseUrl}/

    renderCharset = utf-8

	tx_cooluri_enable = 0
	tx_realurl_enable = 1

    redirectOldLinksToNew = 1

    contentObjectExceptionHandler = 0
}
