TCEFORM.pages {
    layout {
        disabled = 0
        # remove not used layouts 0,1,2,3
        removeItems = 3
        # rename used layouts
        addItems {

        }
        altLabels {
            0 = Default
            1 = Subpage
            2 = Detailseite
        }
        # default layout
        override = 0
    }
}