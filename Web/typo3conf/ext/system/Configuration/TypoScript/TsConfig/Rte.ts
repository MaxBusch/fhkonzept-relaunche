RTE.default {

    RTEWidthOverride = 80%

    showButtons (
		citation, blockquote, nobr, code, definition, deletedtext, emphasis, insertedtext, unlink, subscript, superscript, bold, italic, underline,
		left, center, right, justifyfull, fontsize, orderedlist, unorderedlist, outdent, indent, formatblock, insertcharacter,
		link, chMode, removeformat, textcolor, bgcolor, image, line, blockstyle
    )

    toolbarOrder (
		bold, italic, underline, nobr, bar, subscript, superscript, bar, citation, blockquote, code, definition, bar, left, center, right, justifyfull, bar, orderedlist, unorderedlist, outdent, indent, bar, textcolor, linebreak,
		insertcharacter, bar, link, unlink, bar, chMode, bar, removeformat, bar, image, line, linebreak,
		fontsize, fontstyle, space, formatblock, space, blockstyle
    )

    #add video button
    buttons.link.properties.class.allowedClasses := addToList(headerButton)

    proc.allowedClasses := addToList(video)
    buttons.blockstyle.tags.div.allowedClasses := addToList(video)
}