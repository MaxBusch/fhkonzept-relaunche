lib.forms.newsletterSubscription = CONTENT
lib.forms.newsletterSubscription {
	table = tt_content
	select {
		pidInList = {$plugin.tx_system.settings.newsletter_subscription_form}
		where = colPos = 0
		orderBy = sorting
	}
}