lib.content.zero = COA
lib.content.zero {
    wrap = |

    10 = CONTENT
    10 {
        table = tt_content
        select.orderBy = sorting
        select.where = colPos=0
    }
}

lib.content.one = COA
lib.content.one {
    10 = CONTENT
    10 {
        table = tt_content
        select.orderBy = sorting
        select.where = colPos=1
        slide = -1
    }
    wrap = |
}

lib.content.two = COA
lib.content.two {
    10 = CONTENT
    10 {
        table = tt_content
        select.orderBy = sorting
        select.where = colPos=2
    }
    wrap = |
}

lib.content.three = COA
lib.content.three {
    10 = CONTENT
    10 {
        table = tt_content
        select.orderBy = sorting
        select.where = colPos=3
    }
    wrap = |
}
