#APPSTORE
lib.link.appstore = IMAGE
lib.link.appstore {
	file = typo3conf/ext/system/Resources/Public/Images/appstore.png

	imageLinkWrap = 1
	imageLinkWrap {
		enable = 1
		typolink.parameter = {$plugin.tx_system.settings.appstore_pid}
		typolink.ATagParams = class="apple" target="_blank"
	}
}

#PLAYSTORE
lib.link.playstore = IMAGE
lib.link.playstore {
	file = typo3conf/ext/system/Resources/Public/Images/playstore.png

	imageLinkWrap = 1
	imageLinkWrap {
		enable = 1
		typolink.parameter = {$plugin.tx_system.settings.playstore_pid}
		typolink.ATagParams = class="android" target="_blank"
	}
}