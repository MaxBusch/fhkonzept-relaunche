# lib for logo file configured in the constants
lib.logo = TEXT
lib.logo.value = {$plugin.tx_system.view.imagesRootPath}{$plugin.tx_system.settings.logo}

# replace or adjust special characters or umlauts
temp.titleSectionId = TEXT
temp.titleSectionId {
    field = title
    trim = 1
    case = lower
    replacement {
        10 {
            search.char = 32
            replace = -
        }
        20 {
            search = /(ä|Ä)/
            useRegExp = 1
            replace = ae
        }
        30 {
            search = /(ö|Ö)/
            useRegExp = 1
            replace = oe
        }
        40 {
            search = /(ü|Ü)/
            useRegExp = 1
            replace = ue
        }
        50 {
            search = ß
            replace = ss
        }
        60 {
            search = /\/\\\.\:\;\,\&/
            useRegExp = 1
            replace =
        }
    }
}

# get alias of a pid
temp.aliasSectionId = TEXT
temp.aliasSectionId {
    field = alias
    trim = 1
}

lib.menu.main = HMENU
lib.menu.main {
    special = list
    special.value = {$plugin.tx_system.settings.navigation_uids}
    wrap = <nav class="cl-effect-44 nav navbar-nav navbar-right">|</nav>

    1 = TMENU
    1 {
        NO = 1
        NO {
            doNotLinkIt = 1
            linkWrap = <li>|</li>
            stdWrap >
            stdWrap {
                cObject = TEXT
                cObject {
                    field = nav_title // title
                    typolink {
                        parameter = 1
                        section.cObject < temp.aliasSectionId
                        ATagParams = class="menu-link"
                    }
                }
            }
        }
    }
}

lib.menu.main.subpage < lib.menu.main
lib.menu.main.subpage {
    1.NO.stdWrap.cObject.typolink.ATagParams =
}

# to get a dynamic generated multilanguage link target, we build the link ourselfs
lib.menu.buy = COA
lib.menu.buy {

    # load the generated start tag
    10 < temp.shopLink

    # add the link text from BE
    20 = HMENU
    20 {

        special = list
        special.value = {$plugin.tx_system.settings.buy_now_pid}

        wrap = |

        1 = TMENU
        1 {
            NO = 1
            NO {
                doNotLinkIt = 1
            }
        }
    }

    # close the link
    30 = TEXT
    30.value = </a>
}