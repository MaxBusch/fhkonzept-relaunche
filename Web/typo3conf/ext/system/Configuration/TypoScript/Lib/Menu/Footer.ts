lib.menu.footer.keeper = COA
lib.menu.footer.keeper {
	wrap = <nav class="cl-effect-4">|</nav>

	#headline
	10 = TEXT
	10 {
		wrap = <li class="category">|</li>

	}

	#navigation
	20 = HMENU
	20 {
		special = list
		special.value = {$plugin.tx_system.settings.footer_keeper_list}

		1 = TMENU
		1 {
			NO = 1
			NO {
				doNotLinkIt = 1
				linkWrap = <li>|</li>
				stdWrap >
				stdWrap {
					cObject = TEXT
					cObject {
						field = nav_title // title
						typolink {
							parameter = 1
							section.cObject < temp.aliasSectionId
							ATagParams = class="menu-link"
						}
					}
				}
			}
		}
	}
}

lib.menu.footer.keeper.subpage < lib.menu.footer.keeper
lib.menu.footer.keeper.subpage {
	20.1.NO.stdWrap.cObject.typolink.ATagParams =
}

lib.menu.footer.gigaset = COA
lib.menu.footer.gigaset {
	wrap = <ul>|</ul>

	#headline
	10 = TEXT
	10 {
		wrap = <li class="category">|</li>
		data.dataWrap = DB:pages:{$plugin.tx_system.settings.footer_gigaset}:title
	}

	#navigation
	20 = HMENU
	20 {
		special = directory
		special.value = {$plugin.tx_system.settings.footer_gigaset}

		1 = TMENU
		1 {
        NO {
            doNotLinkIt = 1
            linkWrap = <li>|</li>
            stdWrap >
            stdWrap {
                cObject = TEXT
                cObject {
                    field = nav_title // title
                    typolink {
                        parameter = 1
                        section.cObject < temp.aliasSectionId
                        ATagParams = class="menu-link"
                    }
                }
            }
        }
		}
	}
}

lib.menu.footer.service = COA
lib.menu.footer.service {
	wrap = <nav class="cl-effect-4">|</nav>

	#headline
	10 = TEXT
	10 {
		wrap = <li class="category">|</li>

	}

	#navigation
	20 = HMENU
	20 {
		special = directory
		special.value = {$plugin.tx_system.settings.footer_service}

		1 = TMENU
		1 {
			NO = 1
			NO {
				linkWrap = <li>|</li>
			}
		}
	}
}
