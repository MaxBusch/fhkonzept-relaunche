
/**
* system
*
*
* @author  Carsten Walther
* @package system
* @version 6.2.0
*/

lib.menu.language = HMENU
lib.menu.language {

    special = language
    special.value = {$plugin.tx_system.settings.language_uids}
    special.normalWhenNoLanguage = 0

    wrap = <ul class="nav navbar-nav">|</ul>

    1 = TMENU
    1 {
        noBlur = 1
        NO = 1
        NO {
            linkWrap = <li class="mobile-list">|</li>
            stdWrap.override = Deutsch || English
            doNotLinkIt = 1

            stdWrap.typolink.parameter.data = page:uid
            stdWrap.typolink.additionalParams = &L=0 || &L=1
            stdWrap.typolink.addQueryString = 1
            stdWrap.typolink.addQueryString.exclude = L,id,cHash,no_cache
            stdWrap.typolink.addQueryString.method = GET
            stdWrap.typolink.useCacheHash = 1
            stdWrap.typolink.no_cache = 0
            stdWrap.typolink.ATagParams =
        }

        ACT <.NO
        ACT.linkWrap = <li class="active">|</li>

        USERDEF1 < .NO
        USERDEF2 < .ACT
    }
}

lib.menu.selectedLanguageMobile = TEXT
lib.menu.selectedLanguageMobile.value = DE

[globalVar = GP:L = 0]
lib.menu.selectedLanguageMobile = TEXT
lib.menu.selectedLanguageMobile.value = DE
[global]

[globalVar = GP:L = 1]
lib.menu.selectedLanguageMobile = TEXT
lib.menu.selectedLanguageMobile.value = EN
[global]

[globalVar = GP:L = 2]
lib.menu.selectedLanguageMobile = TEXT
lib.menu.selectedLanguageMobile.value = FR
[global]

[globalVar = GP:L = 3]
lib.menu.selectedLanguageMobile = TEXT
lib.menu.selectedLanguageMobile.value = PL
[global]

[globalVar = GP:L = 4]
lib.menu.selectedLanguageMobile = TEXT
lib.menu.selectedLanguageMobile.value = ES
[global]

[globalVar = GP:L = 5]
lib.menu.selectedLanguageMobile = TEXT
lib.menu.selectedLanguageMobile.value = NL
[global]
