;(function($) {

    $.fn.detailPopup = function() {

        if ($(this).length >= 1) {

            $(this).each(function(){
                var currentHtml = $(this).html();
                var newHtml = '<a href="' + $(this).find('img').attr('src') + '">' + currentHtml + '</a>';
                $(this).html(newHtml);
            });

            $(this).magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery:{
                    enabled:true,
                    arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>'
                }
            });
        }
    }

}(jQuery));

;$(document).ready(function($) {
    $('.content.detail .ce-media').detailPopup();
});
