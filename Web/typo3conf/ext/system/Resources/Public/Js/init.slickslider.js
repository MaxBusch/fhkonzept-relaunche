$(document).ready(function(){
    $(function () {
        var slickOpts = {
            dots: true,
            infinite: true,
            speed: 500,
            autoplay: true,
            autoplaySpeed: 7000,
            arrows: true,
            touchMove: true,
            fade: true,
            prevArrow: '<button type="button" class="arrow-prev slick-prev"></button>',
            nextArrow: '<button type="button" class="arrow-next slick-next"></button>',
            responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    arrows: false
                  }
                }
              ]            
        };
        // Init the slick
        $('.referenzen-slider').slick(slickOpts);
        var slickEnabled = true;
    });

    $(function () {
        var slickOpts = {
            dots: true,
            infinite: true,
            speed: 500,
            autoplay: true,
            autoplaySpeed: 7000,
            arrows: true,
            accessibility: true,
            touchMove: true,
            fade: true,
            prevArrow: '<button type="button" class="arrow-prev slick-prev"></button>',
            nextArrow: '<button type="button" class="arrow-next slick-next"></button>',
            responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    arrows: false
                  }
                }
              ]            
        };
        // Init the slick
        $('.news-slider').slick(slickOpts);
        var slickEnabled = true;
    });

});