var headerHeight = 0;
var navbarShrinked = false;

//var videoOriginalWidth = 0;
//var videoOriginalHeight = 0;

$(document).ready(function(){
	var smartphoneWidth = 767;
	if ($(window).width() > smartphoneWidth) {
		//move newsletter legal to get 100% width
		if($('.newsletter-legal').length) {
			$('.newsletter-legal').insertAfter('.last-news-input');
		}
	}

	headerHeight = $('.tx-headerslider').outerHeight();

	// bind click event to navbar-toggle to expand navigation, when shrinked
	$('.navbar-toggle').click(function(){

		if($('.navbar').hasClass('shrink')) {
			$('.navbar').removeClass('shrink');
		}

		else {
			if(navbarShrinked == true) {
				$('.navbar').addClass('shrink');
			}
		}
	});

	// smooth scrolling
	$('.menu-link').click(function(event){
		smoothScroll(this,event);
	});

	// smooth scrolling
	$('.headerButton').click(function(event){
		smoothScroll(this,event);
	});

	// resize video
	resizeVideoInit();

	// init toggle handling
	$('.navbar-trigger').on('click', function() {
		var target = $(this).data('target');

		// delete # from target
		target = target.replace("#", "");

		// loop through every collapse navbar
		$('.navbar-trigger-target').each(function () {
			var id = $(this).attr('id');

			// if this navbar is not the targeted once, collapse it
			if (id != target) {
				$(this).removeClass('in')
				$(this).attr('aria-expanded','false');
			}
		});
	});

	// init language toggle button
	$('.language-toggle').on('click', function () {
		$('#language-list').toggleClass('in');
	});

	// init scroll top
	$('.brand-anchor').click(function(){
		$('html,body').animate({scrollTop : 0},400);
		return false;
	});

	// init faq scrolling
	if($('.faq-element').length) {
		$('.faq-element').click(function(){
			$(this).children('.answer').slideToggle(400);
			$(this).children('.question').toggleClass('show');
		});
	}

	// init newsletter form validation
	$("#newsletter-subscription").submit(function(event) {
		var error = false;

		// Email
		if(!validateEmail($("#email").val())){
			error = true;
			$('#newsletter-subscription #email').addClass('error');
		}
		else {
			if($('#newsletter-subscription #email').hasClass('error')) {
				$('#newsletter-subscription #email').removeClass('error')
			}
		}

		// Legals
		if($('.newsletter-legal #legal').is(':checked')) {
			if($('.newsletter-legal #legal').hasClass('error')) {
				$('.newsletter-legal #legal').removeClass('error')
			}
		}

		else {
			error = true;
			$('.newsletter-legal #legal').addClass('error');
		}

		// Output
		if(error == true) {
			event.preventDefault();
		}
	});

	if($('.video-wrap').length) {
		// move button into ce-media
		$('.video-wrap .videoButton').prependTo('.video-wrap .ce-media');

		var videoContainer = $('.video-wrap iframe');

		$('.video-wrap .videoButton').on('click', function(ev) {
			// reload iframe with attribute autoplay
			$(videoContainer)[0].src += "&autoplay=1&rel=0";
			ev.preventDefault();

			// hide play button
			$(this).hide();
		});
	}
});

// bind resizing of device to call function
$(window).resize(function() {
	// resize video
	resizeVideoInit();

	var smartphoneWidth = 767;
	if ($(window).width() <= smartphoneWidth) {
		//move newsletter legal to get 100% width
		if($('.newsletter-legal').length) {
			$('.newsletter-legal').insertAfter('.last-news-input .form-actions');
		}
	}

	else {
		//move newsletter legal to get 100% width
		if($('.newsletter-legal').length) {
			$('.newsletter-legal').insertAfter('.last-news-input');
		}
	}
});

function validateEmail(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}

function smoothScroll(element, event) {
	event.preventDefault();

	// get the href attribute from clicked link
	var target = $.attr(element, 'href');

	// get the part after the # to eleminate not needed url parts (e.g. language parameter)
	target = '#' + target.substring(target.indexOf("#")+1);

	// check if target exists
	if($(target).length) {
		// scroll to target
		$('html, body').animate({
			scrollTop: $(target).offset().top
		}, 800);

		// hide navbar
		$('.navbar-trigger-target').each(function() {
			if($(this).hasClass('in')) {
				$(this).removeClass('in')
				$(this).attr('aria-expanded','false');
			}
		});
	}
}

// set scroll listener to window
$(window).on('scroll touchmove', function(){

	var viewportTop = $(window).scrollTop();

	// if the top of the website is reached - bind scolling to headerslider
	if(viewportTop == 0) {
		if(headerslider.binding == false) {
			headerslider.scrollBind();
		}

		$('.navbar').removeClass('shrink');
	}

	// else - go back to regular scrolling
	else {

		if (headerslider.binding == true) {
			headerslider.scrollUnbind();
		}

		if (viewportTop >= headerHeight/2 || viewportTop >= 250) {
			$('.navbar').addClass('shrink');


			$('.navbar-trigger-target').each(function() {
				if($(this).hasClass('in')) {
					$(this).removeClass('in')
					$(this).attr('aria-expanded','false');
				}
			});
		}

		else {
			$('.navbar').removeClass('shrink');
		}
	}
});

// function to check if iframe is loaded by using a 1sec timeout - if iframe is not loaded, the function calls itself to check again
function resizeVideoInit(){
	setTimeout(function(){
		if($('.video-wrap iframe').length) {
			resizeVideo();
		}

		else {
			resizeVideoInit();
		}
	}, 500);
}

// function to do the resize process
function resizeVideo() {

	// get iframe element
	var video = $('.video-wrap iframe');
	// get viewport width
	var viewportWidth = $(window).width();

	if(viewportWidth > 1280) {
		viewportWidth = 1280;
	}

	// get actual iframe width
	var iframeWidth = video.width();
	// get actual iframe height
	var iframeHeight = video.height();
	// calc width/height ratio
	var iframeRatio = iframeWidth/iframeHeight;
	// calc new iframe height based on viewport and iframe ratio
	var newIframeHeight = viewportWidth/iframeRatio;

	video.width(viewportWidth);
	video.height(newIframeHeight);
}