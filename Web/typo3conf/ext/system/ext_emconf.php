<?php

$EM_CONF[$_EXTKEY] = array(
	'title' => 'System',
	'description' => 'System ... :)',
	'category' => 'be',
	'version' => '7.6.4',
	'state' => 'beta',
	'uploadfolder' => false,
	'createDirs' => '',
	'clearcacheonload' => true,
	'author' => 'Carsten Walther',
	'author_email' => 'info@carstenwalther.de',
	'author_company' => '',
	'constraints' => array(
		'depends' => array(
			'typo3' => '7.6.99-8.2.99',
		),
		'conflicts' => array(),
		'suggests' => array(),
	),
);
