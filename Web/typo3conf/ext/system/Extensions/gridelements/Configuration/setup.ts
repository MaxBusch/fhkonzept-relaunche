tt_content.shortcut.variables.shortcuts {
    tables := addToList(pages)
    conf.pages < lib.tt_content.shortcut.pages
}

tt_content.gridelements_pi1.10 =< lib.stdheader
tt_content.gridelements_pi1.20.10.setup {

    container < lib.gridelements.defaultGridSetup
    container {
        cObject = FLUIDTEMPLATE
        cObject {
            templateRootPath = EXT:system/Extensions/gridelements/Resources/Private/Templates/
            partialRootPath = EXT:system/Extensions/gridelements/Resources/Private/Partials/
            layoutRootPath = EXT:system/Extensions/gridelements/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/container.html
        }
    }

    col_1 < lib.gridelements.defaultGridSetup
    col_1 {
        cObject = FLUIDTEMPLATE
        cObject {
            templateRootPath = EXT:system/Extensions/gridelements/Resources/Private/Templates/
            partialRootPath = EXT:system/Extensions/gridelements/Resources/Private/Partials/
            layoutRootPath = EXT:system/Extensions/gridelements/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/col_1.html
        }
    }

    col_2 < lib.gridelements.defaultGridSetup
    col_2 {
        cObject = FLUIDTEMPLATE
        cObject {
            templateRootPath = EXT:system/Extensions/gridelements/Resources/Private/Templates/
            partialRootPath = EXT:system/Extensions/gridelements/Resources/Private/Partials/
            layoutRootPath = EXT:system/Extensions/gridelements/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/col_2.html
        }
    }

    col_3 < lib.gridelements.defaultGridSetup
    col_3 {
        cObject = FLUIDTEMPLATE
        cObject {
            templateRootPath = EXT:system/Extensions/gridelements/Resources/Private/Templates/
            partialRootPath = EXT:system/Extensions/gridelements/Resources/Private/Partials/
            layoutRootPath = EXT:system/Extensions/gridelements/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/col_3.html
        }
    }

    col_3-2 < lib.gridelements.defaultGridSetup
    col_3-2 {
        cObject = FLUIDTEMPLATE
        cObject {
            templateRootPath = EXT:system/Extensions/gridelements/Resources/Private/Templates/
            partialRootPath = EXT:system/Extensions/gridelements/Resources/Private/Partials/
            layoutRootPath = EXT:system/Extensions/gridelements/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/col_3-2.html
        }
    }

    col_2-3 < lib.gridelements.defaultGridSetup
    col_2-3 {
        cObject = FLUIDTEMPLATE
        cObject {
            templateRootPath = EXT:system/Extensions/gridelements/Resources/Private/Templates/
            partialRootPath = EXT:system/Extensions/gridelements/Resources/Private/Partials/
            layoutRootPath = EXT:system/Extensions/gridelements/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/col_2-3.html
        }
    }
    col_2-2 < lib.gridelements.defaultGridSetup
    col_2-2 {
        cObject = FLUIDTEMPLATE
        cObject {
            templateRootPath = EXT:system/Extensions/gridelements/Resources/Private/Templates/
            partialRootPath = EXT:system/Extensions/gridelements/Resources/Private/Partials/
            layoutRootPath = EXT:system/Extensions/gridelements/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/col_2-2.html
        }
    }

    col_4 < lib.gridelements.defaultGridSetup
    col_4 {
        cObject = FLUIDTEMPLATE
        cObject {
            templateRootPath = EXT:system/Extensions/gridelements/Resources/Private/Templates/
            partialRootPath = EXT:system/Extensions/gridelements/Resources/Private/Partials/
            layoutRootPath = EXT:system/Extensions/gridelements/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/col_4.html
        }
    }

    colored_background < lib.gridelements.defaultGridSetup
    colored_background {
        cObject = FLUIDTEMPLATE
        cObject {
            templateRootPath = EXT:system/Extensions/gridelements/Resources/Private/Templates/
            partialRootPath = EXT:system/Extensions/gridelements/Resources/Private/Partials/
            layoutRootPath = EXT:system/Extensions/gridelements/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/colored_background.html
        }
    }

    css_class_container < lib.gridelements.defaultGridSetup
    css_class_container {
        cObject = FLUIDTEMPLATE
        cObject {
            templateRootPath = EXT:system/Extensions/gridelements/Resources/Private/Templates/
            partialRootPath = EXT:system/Extensions/gridelements/Resources/Private/Partials/
            layoutRootPath = EXT:system/Extensions/gridelements/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/css_class_container.html
        }
    }

    navigation < lib.gridelements.defaultGridSetup
    navigation {
        cObject = FLUIDTEMPLATE
        cObject {
            templateRootPath = EXT:system/Extensions/gridelements/Resources/Private/Templates/
            partialRootPath = EXT:system/Extensions/gridelements/Resources/Private/Partials/
            layoutRootPath = EXT:system/Extensions/gridelements/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/navigation.html
        }
    }

    tabs < lib.gridelements.defaultGridSetup
    tabs {
        cObject = FLUIDTEMPLATE
        cObject {
            templateRootPath = EXT:system/Extensions/gridelements/Resources/Private/Templates/
            partialRootPath = EXT:system/Extensions/gridelements/Resources/Private/Partials/
            layoutRootPath = EXT:system/Extensions/gridelements/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/tabs.html
        }
    }

    carousel < lib.gridelements.defaultGridSetup
    carousel {
        cObject = FLUIDTEMPLATE
        cObject {
            templateRootPath = EXT:system/Extensions/gridelements/Resources/Private/Templates/
            partialRootPath = EXT:system/Extensions/gridelements/Resources/Private/Partials/
            layoutRootPath = EXT:system/Extensions/gridelements/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/carousel.html
        }
    }

    header < lib.gridelements.defaultGridSetup
    header {
        cObject = FLUIDTEMPLATE
        cObject {
            templateRootPath = EXT:system/Extensions/gridelements/Resources/Private/Templates/
            partialRootPath = EXT:system/Extensions/gridelements/Resources/Private/Partials/
            layoutRootPath = EXT:system/Extensions/gridelements/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/header.html
        }
    }

    headerslide < lib.gridelements.defaultGridSetup
    headerslide {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:system/Resources/Private/Templates/Grid
            partialRootPath = EXT:system/Resources/Private/Partials/
            layoutRootPath = EXT:system/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/headerslide.html
        }
    }

    features < lib.gridelements.defaultGridSetup
    features {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:system/Resources/Private/Templates/Grid
            partialRootPath = EXT:system/Resources/Private/Partials/
            layoutRootPath = EXT:system/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/features.html
        }
    }

    featuresFish < lib.gridelements.defaultGridSetup
    featuresFish {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:system/Resources/Private/Templates/Grid
            partialRootPath = EXT:system/Resources/Private/Partials/
            layoutRootPath = EXT:system/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/features-fish.html
        }
    }

    featuresPortfolio < lib.gridelements.defaultGridSetup
    featuresPortfolio {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:system/Resources/Private/Templates/Grid
            partialRootPath = EXT:system/Resources/Private/Partials/
            layoutRootPath = EXT:system/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/features-portfolio.html
        }
    }

    featuresslide < lib.gridelements.defaultGridSetup
    featuresslide {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:system/Resources/Private/Templates/Grid
            partialRootPath = EXT:system/Resources/Private/Partials/
            layoutRootPath = EXT:system/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/featuresslide.html
        }
    }

    featuresslideFish < lib.gridelements.defaultGridSetup
    featuresslideFish {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:system/Resources/Private/Templates/Grid
            partialRootPath = EXT:system/Resources/Private/Partials/
            layoutRootPath = EXT:system/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/featuresslideFish.html
        }
    }

    standorte < lib.gridelements.defaultGridSetup
    standorte {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:system/Resources/Private/Templates/Grid
            partialRootPath = EXT:system/Resources/Private/Partials/
            layoutRootPath = EXT:system/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/standorte.html
        }
    }

    onepager < lib.gridelements.defaultGridSetup
    onepager {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:system/Resources/Private/Templates/Grid
            partialRootPath = EXT:system/Resources/Private/Partials/
            layoutRootPath = EXT:system/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/onepager.html
        }
    }

    spacercontainer < lib.gridelements.defaultGridSetup
    spacercontainer {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:system/Resources/Private/Templates/Grid
            partialRootPath = EXT:system/Resources/Private/Partials/
            layoutRootPath = EXT:system/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/spacercontainer.html
        }
    }

    videowrap < lib.gridelements.defaultGridSetup
    videowrap {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:system/Resources/Private/Templates/Grid
            partialRootPath = EXT:system/Resources/Private/Partials/
            layoutRootPath = EXT:system/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/videowrap.html
        }
    }

    news_slider < lib.gridelements.defaultGridSetup
    news_slider {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:system/Resources/Private/Templates/Grid
            partialRootPath = EXT:system/Resources/Private/Partials/
            layoutRootPath = EXT:system/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/news_slider.html
        }
    }

    referenzen < lib.gridelements.defaultGridSetup
    referenzen {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:system/Resources/Private/Templates/Grid
            partialRootPath = EXT:system/Resources/Private/Partials/
            layoutRootPath = EXT:system/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/referenzen.html
        }
    }

    referenzen_slider < lib.gridelements.defaultGridSetup
    referenzen_slider {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:system/Resources/Private/Templates/Grid
            partialRootPath = EXT:system/Resources/Private/Partials/
            layoutRootPath = EXT:system/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/referenzen_slider.html
        }
    }

    news_gallery < lib.gridelements.defaultGridSetup
    news_gallery {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:system/Resources/Private/Templates/Grid
            partialRootPath = EXT:system/Resources/Private/Partials/
            layoutRootPath = EXT:system/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/news-gallery.html
        }
    }

    news_gallery_image_only < lib.gridelements.defaultGridSetup
    news_gallery_image_only {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:system/Resources/Private/Templates/Grid
            partialRootPath = EXT:system/Resources/Private/Partials/
            layoutRootPath = EXT:system/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/news-gallery-image-only.html
        }
    }

    news_one < lib.gridelements.defaultGridSetup
    news_one {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:system/Resources/Private/Templates/Grid
            partialRootPath = EXT:system/Resources/Private/Partials/
            layoutRootPath = EXT:system/Resources/Private/Layouts/

            file = EXT:system/Extensions/gridelements/Resources/Private/Templates/news_one.html
        }
    }

}
