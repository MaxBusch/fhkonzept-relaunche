tx_gridelements {

    # boolean; Usually if the ID of the TSconfig is the same like the record ID of grid elements,
    # the configuration of the TSconfig overrides the record configuration recursively (!).
    # If this option is set the record configuration overrides the TSconfig.
    overruleRecords = 0

    # string; With this option you can disable single layouts. Use the record or TSconfig ID.
    excludeLayoutIds =

    setup {

        container {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:container.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:container.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/container.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 1

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 1
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS = FILE:EXT:system/Extensions/gridelements/Configuration/Flexforms/container.xml
        }

        col_1 {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:col_1.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:col_1.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/col_1.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 1
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS = FILE:EXT:system/Extensions/gridelements/Configuration/Flexforms/col_1.xml
        }

        col_2 {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:col_2.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:col_2.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/col_2.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 2
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 1
                            }
                            2 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_2
                                colPos = 2
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS = FILE:EXT:system/Extensions/gridelements/Configuration/Flexforms/col_2.xml
        }

        col_3 {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:col_3.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:col_3.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/col_3.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 3
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 1
                            }
                            2 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_2
                                colPos = 2
                            }
                            3 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_3
                                colPos = 3
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS = FILE:EXT:system/Extensions/gridelements/Configuration/Flexforms/col_3.xml
        }

        col_3-2 {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:col_3-2.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:col_3-2.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/col_3.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
            	colCount = 3
            	rowCount = 2
            	rows {
            		1 {
            			columns {
            				1 {
            					name = links
            					rowspan = 2
            					colPos = 1
            				}
            				2 {
            					name = oben
                                colPos = 2
            				}
            			}
            		}
            		2 {
            			columns {
            				1 {
            					name = unten
                                colPos = 3
            				}
            			}
            		}
            	}
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            #flexformDS = FILE:EXT:system/Extensions/gridelements/Configuration/Flexforms/col_3-2.xml
        }


        col_2-3 {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:col_2-3.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:col_2-3.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/col_3.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
            	colCount = 3
            	rowCount = 2
            	rows {
                2 {
                    columns {
                        1 {
                            name = unten
                            colPos = 3
                        }
                    }
                }
                    1 {
            			columns {
            				1 {
                            name = oben
                            colPos = 2
            				}
            				2 {
                            name = rechts
                            rowspan = 2
                            colPos = 1

            				}
            			}
            		}

            	}
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            #flexformDS = FILE:EXT:system/Extensions/gridelements/Configuration/Flexforms/col_2-3.xml
        }

        col_2-2 {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:col_2-2.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:col_2-2.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/col_3.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 2
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = oben
                                colPos = 1
                            }
                            2 {
                                name = unten
                                colPos = 2
                            }

                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            #flexformDS = FILE:EXT:system/Extensions/gridelements/Configuration/Flexforms/col_2-3.xml
        }

        col_4 {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:col_4.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:col_4.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/col_4.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 4
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 1
                            }
                            2 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_2
                                colPos = 2
                            }
                            3 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_3
                                colPos = 3
                            }
                            4 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_4
                                colPos = 4
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS = FILE:EXT:system/Extensions/gridelements/Configuration/Flexforms/col_3.xml
        }

        colored_background {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:colored_background.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:colored_background.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/colored_background.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 1
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS = FILE:EXT:system/Extensions/gridelements/Configuration/Flexforms/colored_background.xml
        }

        css_class_container {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:css_class_container.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:css_class_container.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/container.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 2

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 1
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS = FILE:EXT:system/Extensions/gridelements/Configuration/Flexforms/css_class_container.xml
        }

        navigation {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:navigation.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:navigation.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/navigation.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 2

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS = FILE:EXT:system/Extensions/gridelements/Configuration/Flexforms/navigation.xml
        }

        tabs {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:tabs.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:tabs.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/tabs.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 4
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 1
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS = FILE:EXT:system/Extensions/gridelements/Configuration/Flexforms/tabs.xml
        }

        carousel {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:carousel.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:carousel.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/carousel.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 4
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 1
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS = FILE:EXT:system/Extensions/gridelements/Configuration/Flexforms/carousel.xml
        }

        header {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:header.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:header.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/carousel.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 4
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 1
                            }
                        }
                    }
                }
            }
        }

        features {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:features.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:features.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/col_1.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = Features
                                colPos = 1
                            }
                        }
                    }
                }
            }
        }

        featuresFish {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:featuresFish.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:featuresFish.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/col_1.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 5
                rows {
                    1 {
                        columns {
                            1 {
                                name = Headline
                                colPos = 1
                            }
                        }
                    }
                    2 {
                        columns {
                            1 {
                                name = 0x1
                                colPos = 2
                            }
                        }
                    }
                    3 {
                        columns {
                            1 {
                                name = 0x2
                                colPos = 3
                            }
                        }
                    }
                    4 {
                        columns {
                            1 {
                                name = 0x3
                                colPos = 4
                            }
                        }
                    }
                    5 {
                        columns {
                            1 {
                                name = 0x4
                                colPos = 5
                            }
                        }
                    }
                }
            }

        }

        featuresPortfolio {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:featuresPortfolio.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:featuresPortfolio.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/col_1.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = Features
                                colPos = 1
                            }
                        }
                    }
                }
            }
        }

        featuresslide {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:featuresslide.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:featuresslide.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/col_1.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 2
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 1
                            }
                            2 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_2
                                colPos = 2
                            }
                        }
                    }
                }
            }
        }

        featuresslideFish {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:featuresslideFish.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:featuresslideFish.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/col_1.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 2
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 1
                            }
                            2 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_2
                                colPos = 2
                            }
                        }
                    }
                }
            }
        }

        standorte {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:standorte.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:standorte.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/col_3.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 4
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 1
                            }
                            2 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_2
                                colPos = 2
                            }
                            3 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_3
                                colPos = 3
                            }
                            4 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_4
                                colPos = 4
                            }
                        }
                    }
                }
            }
        }

        headerslide {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:headerslide.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:headerslide.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/carousel.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 2
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:contentelement
                                colPos = 1
                            }
                            2 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:imagelement
                                colPos = 2
                            }
                        }
                    }
                }
            }
        }

        onepager {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:onepager.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:onepager.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/container.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 0

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 0
                            }
                        }
                    }
                }
            }
        }

        spacercontainer {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:spacercontainer.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:spacercontainer.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/container.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 0

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 0
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS = FILE:EXT:system/Extensions/gridelements/Configuration/Flexforms/spacercontainer.xml
        }

        videowrap {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:videowrap.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:videowrap.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/col_1.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 0

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 0
                            }
                        }
                    }
                }
            }
        }

        news_slider {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:newsslider.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:newsslider.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/container.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 2

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 4
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 1
                            }
                            2 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_2
                                colPos = 2
                            }
                            3 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_3
                                colPos = 3
                            }
                            4 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_4
                                colPos = 4
                            }
                        }
                    }
                }
            }
        }

        referenzen {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:referenzen.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:referenzen.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/container.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 1

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = Referenzen
                                colPos = 1
                            }
                        }
                    }
                }
            }
        }

        referenzen_slider {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:referenzen_slider.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:referenzen_slider.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/container.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 2
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = Text
                                colPos = 1
                            }
                            2 {
                                name = Bild
                                colPos = 2
                            }
                        }
                    }
                }
            }
        }

        news_gallery {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:news_gallery.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:news_gallery.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/container.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 2
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = oben links
                                rowspan = 2
                                colPos = 0
                            }
                            2 {
                                name = oben rechts
                                colPos = 1
                            }
                        }
                    }
                }
            }

        }

        news_gallery_image_only {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:news_gallery_image_only.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:news_gallery_image_only.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/container.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = Image Only
                                colPos = 1
                            }
                        }
                    }
                }
            }
        }

        news_one {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:newsone.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:newsone.description

            # string; "EXT:" can be used here.
            icon = EXT:system/Extensions/gridelements/Resources/Public/Icons/col_1.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:system/Extensions/gridelements/Resources/Private/Languages/locallang.xlf:column_1
                                colPos = 1
                            }
                        }
                    }
                }
            }
        }

    }
}
