<?php
namespace CarstenWalther\System\ViewHelpers;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class PageAliasViewHelper
 *
 * @author Christian Reifenscheid
 * @package namespace CarstenWalther\System\ViewHelpers;
 */
class PageAliasViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
	/**
	 * @param int $pid
	 *
	 * @return mixed
	 */
    public function render($pid)
    {
    	/** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
		$objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

		/** @var \TYPO3\CMS\Frontend\Page\PageRepository $pagesRepository */
		$pagesRepository = $objectManager->get('TYPO3\\CMS\\Frontend\\Page\\PageRepository');

		$page = $pagesRepository->getPage($pid);
		$alias = GeneralUtility::strtolower($page['alias']);

		return $alias;
	}
}