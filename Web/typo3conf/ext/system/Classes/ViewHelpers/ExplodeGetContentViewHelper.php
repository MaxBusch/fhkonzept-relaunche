<?php
namespace CarstenWalther\System\ViewHelpers;

/**
 * Class ExplodeGetContentViewHelper
 * @package CarstenWalther\System\ViewHelpers
 */
class ExplodeGetContentViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @param string $string String to explode
     * @return array
     */
    public function render($string = '')
    {
        if (strlen($string) > 0) {

            $result = array();

            $pageUids = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $string);

            foreach ($pageUids as $pageUid) {
                $result[$pageUid] = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord('tt_content', $pageUid, '*');
            }

            return $result;

        }
        else {
            return NULL;
        }
    }
}
