<?php
namespace CarstenWalther\System\Finisher;


class Optivo extends \Typoheads\Formhandler\Finisher\AbstractFinisher
{

	/**
	 * The main method called by the controller
	 *
	 * @return array The probably modified GET/POST parameters
	 */
	public function process()
	{
		//read redirect page
		$redirectPage = $this->utilityFuncs->getSingle($this->settings, 'redirectPage');
		if (!isset($redirectPage)) {
			return $this->gp;
		}
		$this->globals->getSession()->reset();

		$this->utilityFuncs->doRedirectBasedOnSettings($this->settings, $this->gp);
	}

	/**
	 * Method to set GET/POST for this class and load the configuration
	 *
	 * @param array The GET/POST values
	 * @param array The TypoScript configuration
	 * @return void
	 */
	public function init($gp, $tsConfig)
	{
		// cache get/post data
		$this->gp = $gp;

		// cache ts config
		$this->settings = $tsConfig;

		// generate url based on configured auth code and set email address
		$url = 'https://api.broadmail.de/http/form/'.$this->settings['authCode'].'/subscribe';
		$url .= '?bmRecipientId=' . urlencode(utf8_decode($gp['email']));

		if($this->settings['optinId'] != '') {
			$url .= '&bmOptInId='.$this->settings['optinId'];
		}

		// call api to get the response
		$response = @file_get_contents($url);

		// registration was successful
		if($response == 'ok') {
			$this->settings['redirectPage'] = $this->settings['redirectPageOk'];
		}

		// user is already registered
		else if ($response == 'duplicate') {
			$this->settings['redirectPage'] = $this->settings['redirectPageDuplicate'];
		}

		// registration faild
		else {
			$this->settings['redirectPage'] = $this->settings['redirectPageError'];
		}
	}

}