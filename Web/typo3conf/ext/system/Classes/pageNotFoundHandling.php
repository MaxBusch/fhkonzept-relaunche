<?php

use TYPO3\CMS\Core\Utility\GeneralUtility;


class user_pageNotFound {
	/**
	* Redirect to 404 error page with language-ID
	*
	* @param array $params: "currentUrl", "reasonText" and "pageAccessFailureReasons"
	* @param object $tsfeObj: object type "tslib_fe"
	*/
	function pageNotFound(&$params, &$tsfeObj) {

		// Get first realurl configuration array (important for multidomain)
		$realurlConf = array_shift($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']);

		// Look for language configuration
		$language = false;

		foreach($realurlConf['preVars'] as $conf) {

			if($conf['GETvar'] == 'L') {

				if(is_array($conf['valueMap'])) {

					foreach($conf['valueMap'] as $k => $v) {

						// We expect a part like "/de/" in requested url
						if(strpos($params['currentUrl'], '/' . $k . '/') !== false) {
							$language = $v;
							break;
						}
					}
				}
			}
		}

		// Get contents of 404 page
		$contentUrl = GeneralUtility::locationHeaderUrl(sprintf(
			'/index.php?id=%u%s',
			$GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling_redirectPageID'],
			$language ? sprintf('&L=%u', $language) : ''
		));

		$content = GeneralUtility::getUrl($contentUrl);

		// Output 404 page
		if($content) {
			echo $content;
			// No 404 page found: Display TYPO3 error page
		} else {
			$params['reasonText'] .= sprintf(
			' Additionally, %s was not found while trying to retrieve the error document.',
			$contentUrl
			);
			if(strpos($contentUrl, 'https://') === 0) {
			$params['reasonText'] .= ' It seems, your SSL certificate seems not to be accepted.';
			}
			$tsfeObj->pageErrorHandler('', '', $params['reasonText']);
		}
	}
}
?>