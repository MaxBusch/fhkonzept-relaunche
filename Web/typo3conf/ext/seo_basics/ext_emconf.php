<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "seo_basics".
 *
 * Auto generated | Identifier: 86b5e3587e5e2eee693897dd1feb9f47
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Basic SEO Features',
	'description' => 'Adds a separate field for the title-tag per page, easy and SEO-friendly keywords and description editing in a new module as well as a flexible Google Sitemap (XML).',
	'category' => 'fe',
	'version' => '0.9.2',
	'state' => 'stable',
	'uploadfolder' => true,
	'createDirs' => '',
	'clearcacheonload' => true,
	'author' => 'Benni Mack',
	'author_email' => 'benni@typo3.org',
	'author_company' => '',
	'constraints' => 
	array (
		'depends' => 
		array (
			'typo3' => '6.2.0-7.9.99',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
	'_md5_values_when_last_written' => 'a:19:{s:9:"README.md";s:4:"709c";s:13:"composer.json";s:4:"f71f";s:21:"ext_conf_template.txt";s:4:"1ffc";s:12:"ext_icon.png";s:4:"c4fa";s:17:"ext_localconf.php";s:4:"ffce";s:14:"ext_tables.php";s:4:"a7f5";s:14:"ext_tables.sql";s:4:"1f99";s:35:"Classes/BackendModule/SeoModule.php";s:4:"53ae";s:40:"Classes/Controller/SitemapController.php";s:4:"068d";s:30:"Classes/Service/UrlService.php";s:4:"2ed9";s:29:"Classes/Tree/PageTreeView.php";s:4:"fc18";s:37:"Configuration/TCA/Overrides/pages.php";s:4:"6cfa";s:54:"Configuration/TCA/Overrides/pages_language_overlay.php";s:4:"9fcd";s:38:"Configuration/TypoScript/constants.txt";s:4:"d86f";s:34:"Configuration/TypoScript/setup.txt";s:4:"8736";s:33:"Resources/Private/Language/db.xml";s:4:"f4f3";s:40:"Resources/Public/JavaScript/SeoModule.js";s:4:"aae2";s:20:"static/constants.txt";s:4:"7333";s:16:"static/setup.txt";s:4:"1e81";}',
);

?>