<?php
namespace Fhkonzept\Headerslider\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Christian Reifenscheid <reifenscheid@fh-konzept.de>, fh-konzept.de
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Slide
 */
class Slide extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * headline
	 *
	 * @var string
	 */
	protected $headline = '';

	/**
	 * bodytext
	 *
	 * @var string
	 */
	protected $bodytext = '';

	/**
	 * videourl
	 *
	 * @var string
	 */
	protected $videourl = '';

	/**
	 * videotext
	 *
	 * @var string
	 */
	protected $videotext = '';

	/**
	 * imageDesktop
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 * @lazy
	 */
	protected $imageDesktop = NULL;

	/**
	 * imageMobile
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 * @lazy
	 */
	protected $imageMobile = NULL;

	/**
	 * imageMobileLandscape
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 * @lazy
	 */
	protected $imageMobileLandscape = NULL;

	/**
	 * colorTheme
	 *
	 * @var int
	 */
	protected $colorTheme = 0;

	/**
	 * headlineLayout
	 *
	 * @var int
	 */
	protected $headlineLayout = 0;

	/**
	 * Returns the headline
	 *
	 * @return string $headline
	 */
	public function getHeadline() {
		return $this->headline;
	}

	/**
	 * Sets the headline
	 *
	 * @param int $headline
	 * @return void
	 */
	public function setHeadline($headline) {
		$this->headline = $headline;
	}

	/**
	 * Returns the headlineLayout
	 *
	 * @return int $headlineLayout
	 */
	public function getHeadlineLayout() {
		return $this->headlineLayout;
	}

	/**
	 * Sets the headlineLayout
	 *
	 * @param string $headlineLayout
	 * @return void
	 */
	public function setHeadlineLayout($headlineLayout) {
		$this->headlineLayout = $headlineLayout;
	}

	/**
	 * Returns the bodytext
	 *
	 * @return string $bodytext
	 */
	public function getBodytext() {
		return $this->bodytext;
	}

	/**
	 * Sets the bodytext
	 *
	 * @param string $bodytext
	 * @return void
	 */
	public function setBodytext($bodytext) {
		$this->bodytext = $bodytext;
	}

	/**
	 * Returns the videourl
	 *
	 * @return string $videourl
	 */
	public function getVideourl() {
		return $this->videourl;
	}

	/**
	 * Sets the videourl
	 *
	 * @param string $videourl
	 * @return void
	 */
	public function setVideourl($videourl) {
		$this->videourl = $videourl;
	}

	/**
	 * Returns the videotext
	 *
	 * @return string $videotext
	 */
	public function getVideotext() {
		return $this->videotext;
	}

	/**
	 * Sets the videotext
	 *
	 * @param string $videotext
	 * @return void
	 */
	public function setVideotext($videotext) {
		$this->videotext = $videotext;
	}

	/**
	 * Returns the imageDesktop
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $imageDesktop
	 */
	public function getImageDesktop() {
		return $this->imageDesktop;
	}

	/**
	 * Sets the imageDesktop
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $imageDesktop
	 * @return void
	 */
	public function setImageDesktop($imageDesktop) {
		$this->imageDesktop = $imageDesktop;
	}

	/**
	 * Returns the imageMobile
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $imageMobile
	 */
	public function getImageMobile() {
		return $this->imageMobile;
	}

	/**
	 * Sets the imageMobile
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $imageMobile
	 * @return void
	 */
	public function setImageMobile($imageMobile) {
		$this->imageMobile = $imageMobile;
	}

	/**
	 * Returns the imageMobileLandscape
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $imageMobileLandscape
	 */
	public function getImageMobileLandscape() {
		return $this->imageMobileLandscape;
	}

	/**
	 * Sets the imageMobileLandscape
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $imageMobileLandscape
	 * @return void
	 */
	public function setImageMobileLandscape($imageMobileLandscape) {
		$this->imageMobileLandscape = $imageMobileLandscape;
	}

	/**
	 * Returns the colorTheme
	 *
	 * @return int $colorTheme
	 */
	public function getColorTheme() {
		return $this->colorTheme;
	}

	/**
	 * Sets the colorTheme
	 *
	 * @param int $colorTheme
	 * @return void
	 */
	public function setColorTheme($colorTheme) {
		$this->colorTheme = $colorTheme;
	}

}