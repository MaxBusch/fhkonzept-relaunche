<?php

namespace Fhkonzept\Headerslider\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Christian Reifenscheid <reifenscheid@fh-konzept.de>, fh-konzept.de
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Fhkonzept\Headerslider\Domain\Model\Slide.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Christian Reifenscheid <reifenscheid@fh-konzept.de>
 */
class SlideTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \Fhkonzept\Headerslider\Domain\Model\Slide
	 */
	protected $subject = NULL;

	public function setUp() {
		$this->subject = new \Fhkonzept\Headerslider\Domain\Model\Slide();
	}

	public function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getHeadlineReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getHeadline()
		);
	}

	/**
	 * @test
	 */
	public function setHeadlineForStringSetsHeadline() {
		$this->subject->setHeadline('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'headline',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getBodytextReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getBodytext()
		);
	}

	/**
	 * @test
	 */
	public function setBodytextForStringSetsBodytext() {
		$this->subject->setBodytext('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'bodytext',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getColorThemeReturnsInitialValueForInt() {	}

	/**
	 * @test
	 */
	public function setColorThemeForIntSetsColorTheme() {	}

	/**
	 * @test
	 */
	public function getImageDesktopReturnsInitialValueForFileReference() {
		$this->assertEquals(
			NULL,
			$this->subject->getImageDesktop()
		);
	}

	/**
	 * @test
	 */
	public function setImageDesktopForFileReferenceSetsImageDesktop() {
		$fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$this->subject->setImageDesktop($fileReferenceFixture);

		$this->assertAttributeEquals(
			$fileReferenceFixture,
			'imageDesktop',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getImageMobileReturnsInitialValueForFileReference() {
		$this->assertEquals(
			NULL,
			$this->subject->getImageMobile()
		);
	}

	/**
	 * @test
	 */
	public function setImageMobileForFileReferenceSetsImageMobile() {
		$fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$this->subject->setImageMobile($fileReferenceFixture);

		$this->assertAttributeEquals(
			$fileReferenceFixture,
			'imageMobile',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getImageMobileLandscapeReturnsInitialValueForFileReference() {
		$this->assertEquals(
			NULL,
			$this->subject->getImageMobileLandscape()
		);
	}

	/**
	 * @test
	 */
	public function setImageMobileForFileReferenceSetsImageMobileLandscape() {
		$fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$this->subject->setImageMobileLandscape($fileReferenceFixture);

		$this->assertAttributeEquals(
			$fileReferenceFixture,
			'imageMobileLandscape',
			$this->subject
		);
	}

}
