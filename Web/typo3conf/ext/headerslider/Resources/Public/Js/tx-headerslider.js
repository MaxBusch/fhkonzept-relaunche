$(document).ready(function(){
	headerslider.init({
		prevTrigger: '.headerslider-button.prev-slide',
		nextTrigger: '.headerslider-button.next-slide'
	});

	// init lightbox for videoLightbox buttons
	$('.tx-headerslider .videoLightbox').magnificPopup({
		type:'iframe',
		iframe: {
			patterns: {
				youtube: {
					src: '//www.youtube.com/embed/%id%?autoplay=1&rel=0'
				}
			}
		}
	});
});

function videoInit () {
	console.log('re');
}


var headerslider = {

	container : '.tx-headerslider',
	element : '.headerslider-element',
	totalElements : 0,
	prevSlide: -1,
	prevTrigger : '',
	nextSlide: 1,
	nextTrigger : '',
	roadmap: false,
	roadmapContainer: '.headerslider-roadmap-wrap',
	milestone: '.milestone',
	spacer: '.spacer',
	timerSpeed: 2000, // time scrolling is locked after one scroll event
	timeout: false, // flag to prevent scrolling within the locked time range
	binding: true, // prevent multiple useless calls of bind functions
	bounced: false, // flag to bounce just one time

	init : function(settings) {

		// set user settings
		if(settings != null) {
			if (settings.container != null) {
				this.container = settings.container;
			}

			if (settings.element != null) {
				this.element = settings.element;
			}

			if (settings.nextTrigger != null) {
				this.nextTrigger = settings.nextTrigger;
			}

			if (settings.prevTrigger != null) {
				this.prevTrigger = settings.prevTrigger;
			}
		}

		// init roadmap if div is existing
		if ($('.headerslider-roadmap').length) {

			// set flag to true
			this.roadmap = true;

			// init roadmap
			headerslider.initRoadmap();
		}

		// get total amount of elements
		this.totalElements = $(this.element).length;

		// set first element to active
		$(this.element + '[data-uid="0"]').addClass('active');

		// set first roadmap element to active
		if(this.roadmap == true) {
			$(this.milestone + '[data-uid="0"]').addClass('active');
			$(this.spacer + '[data-uid="0"]').addClass('active');
		}

		if (this.nextTrigger != '' || this.prevTrigger != '') {
			// check trigger
			headerslider.checkTrigger();

			// bind click events
			if (this.prevTrigger != '') {
				$(this.prevTrigger).click(function(){
					headerslider.prev('click');
				});
			}

			if (this.nextTrigger) {
				$(this.nextTrigger).click(function(){
					headerslider.next('click');
				});
			}
		}

		// init binding if viewport top = 0
		if($(window).scrollTop() == 0) {
			headerslider.scrollBind();
		}
	},

	scrollBind : function () {
		this.binding = true;
		$(document).bind('mousewheel DOMMouseScroll MozMousePixelScroll', function(event) {

			event.preventDefault();
			var delta = event.originalEvent.wheelDelta || -event.originalEvent.detail;

			// delta < 0 = scrolling down
			if (delta < 0) {
				// if the next slide is not the last one - show next slide
				if (headerslider.nextSlide < headerslider.totalElements) {
					// just call function if timeout is not set - timeout prevents multiple scroll events by using the scroll wheel once
					if (headerslider.timeout == false) {
						headerslider.next('scroll');
					}
				}

				// else if the next slide is the last, unbind scroll events
				else {
					headerslider.scrollUnbind();
				}
			}

			// scrolling up
			else {
				// if the previous slide is not the first one - show previous slide
				if (headerslider.prevSlide > 0) {
					if (headerslider.timeout == false) {
						headerslider.prev('scroll');
					}
				}
			}
		});
	},

	scrollUnbind : function () {
		$(document).unbind('mousewheel DOMMouseScroll MozMousePixelScroll');
		this.binding = false;
	},

	/**
	 * show next slide
	 *
	 * @param mod
	 */

	next : function (mod) {
		// add active class to next slide
		$(this.element + '[data-uid="' + this.nextSlide + '"]').addClass('active');

		// set roadmap element to active
		if(this.roadmap == true) {
			$(this.milestone + '[data-uid="' + this.nextSlide + '"]').addClass('active');
			$(this.spacer + '[data-uid="' + this.nextSlide + '"]').addClass('active');
		}

		// set next slide
		this.nextSlide = this.nextSlide + 1;

		// set previous slide
		this.prevSlide = this.nextSlide - 1;

		// check trigger
		if (this.nextTrigger != '' || this.prevTrigger != '') {
			headerslider.checkTrigger();
		}

		// set timeout just for scroll events
		if (mod == 'scroll') {
			// set timeout to true to force a timeout
			this.timeout = true;

			// reset timeout
			headerslider.resetTimeout();
		}
	},

	/**
	 * show previous slide
	 *
	 * @param mod
	 */
	prev : function (mod) {
		// add active class to next slide
		$(this.element + '[data-uid="' + this.prevSlide + '"]').removeClass('active');

		// set roadmap element to active
		if(this.roadmap == true) {
			$(this.milestone + '[data-uid="' + this.prevSlide + '"]').removeClass('active');
			$(this.spacer + '[data-uid="' + this.prevSlide + '"]').removeClass('active');
		}

		// increment acutalslide
		this.prevSlide = this.prevSlide - 1;

		// set previous slide
		this.nextSlide = this.prevSlide + 1;

		// check trigger
		if (this.nextTrigger != '' || this.prevTrigger != '') {
			headerslider.checkTrigger();
		}

		// set timeout just for scroll events
		if (mod == 'scroll') {
			// set timeout to true to force a timeout
			this.timeout = true;

			// reset timeout
			headerslider.resetTimeout();
		}
	},

	/**
	 * Check if a trigger has to be shown
	 */
	checkTrigger : function () {

		// check if prev trigger has to be shown
		if (this.prevTrigger != '') {
			if (this.prevSlide > 0) {
				$(this.prevTrigger).fadeIn('slow');
			}

			else {
				$(this.prevTrigger).fadeOut('slow');
			}
		}

		// check if next trigger has to be shown
		if (this.nextTrigger != '') {
			if (this.nextSlide < this.totalElements) {
				$(this.nextTrigger).fadeIn('slow');

				// bounce next trigger when it is loaded the first time
				if (this.bounced == false) {
					setTimeout(function(){
						// bounce effect
						$(headerslider.nextTrigger).effect("bounce", {times: 5}, 3000 );

						// fade out hint after bounce
						setTimeout(function(){
							$(headerslider.nextTrigger + ' p.hint').fadeOut('slow');
						}, 3500);

						// set flag to prevent bouncing by upcoming trigger checks
						headerslider.bounced = true;
					}, 1500);
				}
			}

			else {
				$(this.nextTrigger).fadeOut('slow');
			}
		}
	},

	/**
	 * calculate spacer height - roadmap has always the same height, height of spacer differs based on number of slides
	 */
	initRoadmap : function () {
		// get max height = 100%
		var maxHeight = $(this.roadmapContainer).height();

		// get size of milestonse
		var milestoneHeight = $(this.milestone).height();

		// get amount of milestones
		var numMilestones = $(this.milestone).length;

		// calc left height without milestones
		var leftHeight = maxHeight - (numMilestones * milestoneHeight);

		// divide left height through number of spacer (milestones - 1) --> spacer height in px
		var spacerInnerHeight = leftHeight / (numMilestones - 1);

		// get margin of spacer
		var spacerMargin = parseInt($(this.spacer).css('margin-top')) * 2;

		// add margin to get the final height incl. margin bottom and margin top
		var spacerHeight = spacerInnerHeight - spacerMargin;

		// calc percentage height for spacer
		var spacerHeightPercent = Math.floor((spacerHeight/maxHeight) * 100);

		// set calc height of spacer
		$(this.spacer).height(spacerHeightPercent + '%');
	},

	/**
	 * reset timeout to reenable scrolling
	 */

	resetTimeout : function () {
		setTimeout(function(){
			headerslider.timeout = false;
		}, headerslider.timerSpeed);
	}
}