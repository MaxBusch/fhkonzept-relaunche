/* global require */
/**
 * @author Malte Muth <muth@fh-konzept.de>
 *
 * first, run npm install -g gulp, then run npm install in this directory
 */
var
    fs 				= require('fs'),
    path 	        = require('path'),
    print 			= require('gulp-print'),
    gulp 			= require('gulp'),

    glob 			= require('glob'),
    yaml            = require('js-yaml'),

    // SCSS task dependencies
    sass 			= require('gulp-sass'),
    autoprefixer 	= require('gulp-autoprefixer'),

    // angular build task dependencies
    concat          = require('gulp-concat'),
    ngAnnotate      = require('gulp-ng-annotate'),
    gulpNotify      = require('gulp-notify'),
    rename          = require('gulp-rename'),

    // angular html template task dependencies
    templateCache = require('gulp-angular-templatecache'),

    // copy file dependencies
    rsync       = require('gulp-rsync'),

    // npm bundling dependencies,
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),

    configuration = yaml.safeLoad(fs.readFileSync('config.yaml')),

    notify = gulpNotify.withReporter( function (options, callback) {
        // console.log(options.title);
        // console.log(options.message);
        callback();
    }),

    buildBundle = function(sourceFile, filename, target, globalName) {
        var b;
        if(globalName) {
            b = browserify(sourceFile, {standalone: globalName});
        } else {
            b = browserify(sourceFile);
        }

        b.bundle()
            //Pass desired output filename to vinyl-source-stream
            .pipe(source(filename))
            // Start piping stream to tasks!
            .pipe(gulp.dest(target));
    },
    /**
     * process the given fileOrDirectory via scss
     *
     * @todo  refactor this into sassifyFile and sassifyDirectory
     * @todo  move to module
     *
     * @param  {string} fileOrDirectory the file or directory path to be passed to sass()
     */
    sassify = function(fileOrDirectory, target) {
    	var source = fileOrDirectory,
    		pathInfo = fileOrDirectory.split("/"),
    		targetDirectory;

    	var fileName = pathInfo.pop();

    	if( !fs.lstatSync(fileOrDirectory).isDirectory() ) {

    		// filenames starting with "_" won't be compiled, so we resort to directory handling instead
    		if ( "_" === fileName[0] ) {
    			fileOrDirectory = pathInfo.concat(["*.scss"]).join("/");
    		}
    		pathInfo.pop();

    	} else {

    		// sass() does not seem to handle directories
    		fileOrDirectory = fileOrDirectory + "/*.scss";
    	}

    	gulp.src(fileOrDirectory)
    		.pipe(sass())
    		.on('error', function(e) {
    			console.log(e.message);
    		})

    		.pipe(autoprefixer('last 2 version'))
    		.pipe(gulp.dest(target))
    		.pipe(notify(function(p) {
    			return "sassified  : " + target;
    		}));
    },

    copyFiles = function (source, target) {
        gulp.src(source + '/**')
            .pipe(rsync({
                root: source,
                destination: target,
                progress: true,
                incremental: true,
                clean: true,
                recursive: true,
                silent: true
            }))
            .pipe(notify(function(p) {
                return 'copied     : ' + source + ' to ' + target;
            }));
    },

    copyFile = function (source, target) {
        gulp.src(source)
            .pipe(rsync({
                root: path.dirname(source),
                destination: target,
                progress: true,
                silent: true
            }))
            .pipe(notify(function(p) {
                return 'copied     : ' + source + ' to ' + target;
            }));
    },


    minifyApp = function (directory, target, filename, dependencies) {
        var directories = [],
            appFiles = [],
            directoryParts = directory.split("/"),
            directoryName = directoryParts[directoryParts.length - 2];

        // collect all files
        directories = dependencies || [];
        directories.push(directory);

        appFiles = directories.map( function (dir) {
            return ['**/*.js', 'Main.js'].map( function (entry) {
                return dir + entry;
            });
        }).reduce(function(entries, newEntries) {
            return entries.concat(newEntries);
        }, []);

        gulp.src(appFiles)
            .pipe(concat(filename))
            .pipe(ngAnnotate())
            .on('error', notify.onError("Error: <%= error.message %>"))
            .pipe(gulp.dest(target))
            .pipe(notify(function(p) {
    			return "minified   : " + filename;
    		}));
    },

    buildHtmlTemplates = function(source, target) {
        var directoryName = path.basename(path.dirname(source));
        return gulp.src(source + '**/*.html')
            .pipe(templateCache({
                filename: 'Templates.js',
                module: directoryName + '.Templates',
                standalone: true,
                transformUrl: function (url) {
                    return directoryName.toLowerCase() + '.' + url.toLowerCase();
                }
            }))
            .pipe(gulp.dest(target))
            .pipe(notify( function (p) {
                return 'html built : ' + target;
            }));
    },

    watchHtml = function ( source, target ) {
        console.log('[HTML] watching ' + source);

        gulp.watch(source + '/**/*.html', function(e) {
    		buildHtmlTemplates(source, target);
    	});
    },

    watchScss = function ( source, target, additionalWatches ) {
        var watchDirectories = additionalWatches || [];
        watchDirectories.push(source)
        console.log('[CSS] watching ' + watchDirectories);

        watchDirectories.forEach( function (directory) {
            gulp.watch(directory + '/**/*.scss', function(e) {
        		sassify(source, target);
        	});
        })


    },

    watchApp = function ( source, target, filename, additionalWatches ) {
        var watchDirectories = additionalWatches || [];
        watchDirectories.push(source);
        console.log('[JS] watching ' + watchDirectories);

        watchDirectories.forEach( function (directory) {
            gulp.watch(directory + '/**/*.js', function(e) {
        		minifyApp(source, target, filename, additionalWatches);
        	});
        })
    },

    watchFiles = function ( source, target) {
        console.log('[FILES] watching ' + source);
        if(fs.existsSync(source)) {
            if(fs.statSync(source).isDirectory()) {
                gulp.watch(source + '/**/*', function(e) {
                    copyFiles(source, target);
                });
            } else {
                gulp.watch(source, function(e) {
                    copyFile(source, target);
                });
            }
        } else {
            // treat as a glob.
            gulp.watch(source, function(e) {
                copyFile(source, target);
            });
        }
    };

gulp.task('bundles:build', function () {
    console.log("[BUNDLE] building bundles");
    configuration.targets.forEach( function (target) {
        target.assets.filter( function (asset) {
            return asset.type === "bundle";
        }).forEach ( function (asset) {
            buildBundle(asset.source, asset.filename, asset.target, asset.global);
        });
    })
});

gulp.task('html:build', function () {
    console.log("[HTML] building templates");
    configuration.targets.forEach( function (target) {
        target.assets.filter( function (asset) {
            return asset.type === "html";
        }).forEach ( function (asset) {
            buildHtmlTemplates(asset.source, asset.source);
        });
    })
});

gulp.task('html:watch', function () {
    console.log("[HTML] Setting up watches");
    configuration.targets.forEach( function (target) {
        target.assets.filter( function (asset) {
            return asset.type === "html";
        }).forEach ( function (asset) {
            watchHtml(asset.source, asset.source);
        });
    })
});

gulp.task('js:build', function () {
    console.log("[JS] building apps");
    configuration.targets.forEach( function (target) {
        target.assets.filter( function (asset) {
            return asset.type === "js";
        }).forEach ( function (asset) {
            minifyApp(asset.source, asset.target, asset.filename, asset.watches);
        });
    })
});

gulp.task('js:watch', function() {
    console.log("[JS] Setting up  watches");
    configuration.targets.forEach( function (target) {
        console.log(" - Target '" + target.name + "'");
        target.assets.filter( function (asset) {
            return asset.type === "js";
        }).forEach ( function (asset) {
            console.log('-- watching ' + asset.name);
            watchApp(asset.source, asset.target, asset.filename, asset.watches);
        });
    })
});


gulp.task('css:watch', function() {
    console.log("[CSS] Setting up  watches");
    configuration.targets.forEach( function (target) {
        console.log(" - Target '" + target.name + "'");
        target.assets.filter( function (asset) {
            return asset.type === "scss";
        }).forEach ( function (asset) {
            console.log('-- watching ' + asset.name);
            watchScss(asset.source, asset.target, asset.watches);
        });
    })
});

gulp.task('css:build', function() {
    console.log("[CSS] Building CSS files");
    configuration.targets.forEach( function (target) {
        target.assets.filter( function (asset) {
            return asset.type === "scss";
        }).forEach ( function (asset) {
            sassify(asset.source, asset.target);
        });
    })
});

gulp.task('files:watch', function () {
    console.log("[FILES] Setting up watches");
    configuration.targets.forEach( function (target) {
        console.log(" - Target '" + target.name + "'");
        target.assets.filter( function (asset) {
            return asset.type === "copy";
        }).forEach ( function (asset) {
            console.log('-- watching ' + asset.name);
            watchFiles(asset.source, asset.target);
        });
    })
});

gulp.task('files:copy', function () {
    console.log('[FILES] copying files to destination:');
    configuration.targets.forEach( function (target) {
        target.assets.filter( function (asset) {
            return asset.type === "copy";
        }).forEach ( function (asset) {
            if(fs.existsSync(asset.source)) {
                if(fs.statSync(asset.source).isDirectory()) {
                    copyFiles(asset.source, asset.target);
                } else {
                    copyFile(asset.source, asset.target);
                }
            } else {
                // treat as glob
                copyFile(asset.source, asset.target);
            }
        });
    })
});

gulp.task('dumpConfig', function () {
    console.log(configuration);
})

gulp.task('build', ['css:build', 'files:copy', 'html:build', 'js:build', 'bundles:build']);

gulp.task('default', [
    'css:build',
    'files:copy',
    'html:build',
    'js:build',
    'bundles:build',
    'css:watch',
    'files:watch',
    'html:watch',
    'js:watch'
    ]);
